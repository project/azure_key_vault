Azure Key Vault REST API Integration
====================================

This module is providing the REST API integration between Drupal 8/9 and 'Azure Key Vault' using the 'Azure Active Directory' (AAD) token authentication (https://docs.microsoft.com/en-us/azure/key-vault/general/authentication).

Additionally, this module provides another sub module to use 'Azure Key Vault' as a provider in 'Key' module (https://www.drupal.org/project/key), so that it'll provide the lacking GUI in the meantime. Please refer to the documentation of 'Key' module for further details (https://www.drupal.org/docs/contributed-modules/key). Only working with the "Key type: Authentication" for now.

## Available methods:

- createSecret (https://docs.microsoft.com/en-us/rest/api/keyvault/set-secret/set-secret)
- getSecret (https://docs.microsoft.com/en-us/rest/api/keyvault/get-secret)
- getSecrets (https://docs.microsoft.com/en-us/rest/api/keyvault/get-secrets)
- deleteSecret (https://docs.microsoft.com/en-us/rest/api/keyvault/delete-secret)
- purgeSecret (https://docs.microsoft.com/en-us/rest/api/keyvault/purge-deleted-secret)
- getData (https://docs.microsoft.com/en-us/rest/api/keyvault/vaults/get)
- encryptKey (https://docs.microsoft.com/en-us/rest/api/keyvault/encrypt)
- decryptKey (https://docs.microsoft.com/en-us/rest/api/keyvault/decrypt)
- getKey (https://docs.microsoft.com/en-us/rest/api/keyvault/get-key)
- deleteKey (https://docs.microsoft.com/en-us/rest/api/keyvault/delete-key)
- generateKey (https://docs.microsoft.com/en-us/rest/api/keyvault/create-key)

## Example of use:

Install the 'azure_key_vault' module. (For more information on installing modules, visit:
https://www.drupal.org/node/1897420 for further information.)

Navigate the configuration section 'site/admin/config/system/az_key_vault_config' and configure with your Azure Key Vault URL, Token OAuth URL, client ID and client secret.

If you need the secret or key inside a module or theme file (or anywhere you can't use injection),
```php
$secret = \Drupal::service('azure_key_vault.http_client')->getSecret("your_secret_name");
```

If you need the secret or key inside a class where you can use injection, then you'll need below code snippets added in different places of your class.

For references,
```php
use Drupal\azure_key_vault\AzureKeyVaultRequestHandlerInterface;
```

For class properties,
```php
/**
 * Azure Key Vault Request Handler.
 *
 * @var \Drupal\azure_key_vault\AzureKeyVaultRequestHandlerInterface
 */
protected $azkvHandler;
```

For '__construct' function,
```php
/**
 * Constructs a new instance.
 *
 * @param \Drupal\azure_key_vault\AzureKeyVaultRequestHandlerInterface $azkv_handler
 *   The Azure key vault handler.
 */
public function __construct(AzureKeyVaultRequestHandlerInterface $azkv_handler) {
  $this->azkvHandler = $azkv_handler;
}
```

For 'create' function,
```php
/**
 * {@inheritdoc}
 */
public static function create(ContainerInterface $container) {
  return new static($container->get('azure_key_vault.http_client'));
}
```

Where you need the Azure key vault secret,
```php
public function exampleMethod() {
  $secret = $this->azkvHandler->getSecret("your_secret_name");
}
```

Optionally, you can enable the 'azure_key_vault_key_provider' sub module as well, in-case you would like to manage the keys / secrets via 'Key' module GUI. Beware, the functionality is limited here and not fully compatible with all key types the 'Key' module offering.

## Important:

It's recommended to restrict the Azure Key Vault API access to prevent any unauthorised access as the authorisation token secrets might be available in the GIT repository, config files (if you are exporting the config and sync through GIT) and database configuration tables.

As an alternative, you may use the 'Key Configuration Overrides' feature provided by 'Key' module to load these configs directly from 'settings.php' or '.env' file and use the hosting provider's environment variables to inject them into the 'settings.php' / '.env'. This way you'll be keeping the authorisation token secrets only on your hosting server and reducing the risk of having those on multiple locations such as GIT, database and code base.

## ToDo:

- Implement caching to keep the secrets in cache for a certain time period to reduce number of API calls which will help with reducing Azure costs.
- Integrate with additional API functions from Azure Key Vault API (https://docs.microsoft.com/en-us/rest/api/keyvault/)
