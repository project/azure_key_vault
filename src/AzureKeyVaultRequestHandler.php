<?php

namespace Drupal\azure_key_vault;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\ClientInterface;

/**
 * Class AzureKeyVaultRequestHandler.
 *
 * This class is implementing the API requests to Azure Key Vault.
 */
class AzureKeyVaultRequestHandler implements AzureKeyVaultRequestHandlerInterface {

  /**
   * Azure Key Vault configuration.
   *
   * @var AzureKeyVaultConfigurationService
   */
  protected $configService;

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Access Token.
   *
   * @var string
   */
  protected $accessToken;

  /**
   * Constructs a new Azure Key Vault Request Handler object.
   */
  public function __construct(AzureKeyVaultConfigurationService $config_service, ClientInterface $http_client, CacheBackendInterface $cacheBackend, LoggerChannelInterface $logger) {
    $this->configService = $config_service;
    $this->httpClient = $http_client;
    $this->cache = $cacheBackend;
    $this->logger = $logger;

    $this->accessToken = $this->getAccessToken();
  }

  /**
   * Retrieve access token using simple_oauth.
   *
   * @return string
   *   Access token.
   */
  protected function getAccessToken() {
    // Fetch configuration values from the Azure Key Vault configuration service.
    $token_url = $this->configService->getTokenUrl();
    $vault_id = $this->configService->getClientId();
    $vault_secret = $this->configService->getClientSecret();
// $this->logger->error("Testing secret value: %details", ['%details' => $vault_secret]);

    // Define the required scopes.
    $scopes = 'https://vault.azure.net/.default';

    $data = ['access_token' => ''];
    try {
      // Request a new access token using Guzzle.
      $response = $this->httpClient->request('POST', $token_url, [
        'form_params' => [
          'client_id' => $vault_id,
          'client_secret' => $vault_secret,
          'scope' => $scopes,
          'grant_type' => 'client_credentials',
        ],
      ]);

      $data = json_decode($response->getBody(), TRUE);
    }
    catch (ClientException $e) {
      $response = $e->getResponse();
      $responseBodyAsString = $response->getBody()->getContents();
      $data = ['error' => $responseBodyAsString,
        'access_token' => '',
        'http_code' => $e->getCode(),
        'access_token' => $e->getCode(),
      ];
    }
    if (isset($data['error'])) {
      $this->logger->warning("Error getting access token is: %error and http_code is: %http_code.", [
        '%error' => $data['error'],
        '%http_code' => $data['http_code'],
      ]);
    }
    return $data['access_token'];
  }

  /**
   * Validate the details added in config area.
   *
   * @return bool
   *   Return true or false.
   */
  public function validateConfig() {
    if (!$this->accessToken) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Create secret in key vault with given name and value.
   *
   * @param string $secret_name
   *   Name for the secret.
   * @param string $value
   *   Value to store as the secret.
   *
   * @return array
   *   Return the created secret with metadata in an array.
   */
  public function createSecret($secret_name, $value) {
    $request_body = [
      'value' => $value,
    ];
    return $this->sendRequest("SECRET", $secret_name, "PUT", $request_body);
  }

  /**
   * GET secret value from key vault.
   *
   * @param string $secret_name
   *   Name for the secret.
   *
   * @return string
   *   Value of the secret.
   */
  public function getSecret($secret_name) {
    return $this->sendRequest("SECRET", $secret_name, "GET")["value"];
  }

  /**
   * GET a list of secrets from key vault.
   *
   * @return array
   *   List of secrets.
   */
  public function getSecrets() {
    return $this->sendRequest("GET_SECRETS", "", "GET");
  }

  /**
   * Temporarily delete a secret from key vault (recorverable).
   *
   * @param string $secret_name
   *   Name for the secret.
   *
   * @return array
   *   Metadata of deleted secret.
   */
  public function deleteSecret($secret_name) {
    return $this->sendRequest("DELETE_SECRET", $secret_name, "DELETE");
  }

  /**
   * Purge (Permanently deletes) a secret from key vault.
   *
   * @param string $secret_name
   *   Name for the secret.
   *
   * @return array
   *   204 status code or error details.
   */
  public function purgeSecret($secret_name) {
    return $this->sendRequest("PURGE_SECRET", $secret_name, "DELETE");
  }

  /**
   * GET method for generic actions.
   *
   * @param string $full_url
   *   Complete URL for the API call.
   *
   * @return array
   *   Output for the API call.
   */
  public function getData($full_url) {
    return $this->sendRequest("GET", $full_url, "GET");
  }

  /**
   * Encrypts a key.
   *
   * @param string $key_to_be_encrypted
   *   Value to be encrypted.
   * @param string $key_name
   *   Name for the encrypted key.
   *
   * @return string
   *   Entrypted key value.
   */
  public function encryptKey($key_to_be_encrypted, $key_name) {
    $request_body = [
      "alg" => "RSA1_5",
      'value' => $key_to_be_encrypted,
    ];
    return $this->sendRequest("ENCRYPT_KEY", $key_name, "POST", $request_body)["value"];
  }

  /**
   * Decrypts a key.
   *
   * @param string $encrypted_key
   *   Encrypted key string.
   * @param string $key_name
   *   Name of the encrypted key.
   *
   * @return string
   *   Decrypted value.
   */
  public function decryptKey($encrypted_key, $key_name) {
    $request_body = [
      "alg" => "RSA1_5",
      'value' => $encrypted_key,
    ];
    $decrypted_key = $this->sendRequest("DECRYPT_KEY", $key_name, "POST", $request_body)["value"];
    // For some bizzare reason, / becomes _ when encrypted,
    // so we need to reverse it to get the accurate original key.
    $decrypted_key = str_ireplace("/", "_", $decrypted_key);
    $decrypted_key = str_ireplace("_", "/", $decrypted_key);
    $decrypted_key = str_ireplace("+", "-", $decrypted_key);
    $decrypted_key = str_ireplace("-", "+", $decrypted_key);
    return $decrypted_key . "==";
  }

  /**
   * GET key value from key vault.
   *
   * @param string $key_name
   *   Name of the encrypted key.
   *
   * @return string
   *   Encrypted key.
   */
  public function getKey($key_name) {
    // @todo Handle key versions if required in future.
    $key_value = $this->sendRequest("KEY", $key_name, "GET");
    return $key_value['key']['n'];
  }

  /**
   * GENERATE a RSA key in key vault.
   *
   * @param string $key_name
   *   Name of the encrypted key.
   * @param string $key_size
   *   Key size of the RSA key.
   *
   * @return string
   *   Encrypted key.
   */
  public function generateKey($key_name, $key_size) {
    $request_body = [
      'kty' => 'RSA',
      'key_size' => intval($key_size),
      'key_ops' => [
        'encrypt',
        'decrypt',
        'sign',
        'verify',
        'wrapKey',
        'unwrapKey',
      ],
    ];

    $new_value = $this->sendRequest("CREATE_KEY", $key_name, "POST", $request_body);
    return $new_value['key']['n'];
  }

  /**
   * DELETE a key from key vault.
   *
   * @param string $key_name
   *   Name of the encrypted key.
   *
   * @return array
   *   Metadata of deleted key.
   */
  public function deleteKey($key_name) {
    return $this->sendRequest("DELETE_KEY", $key_name, "DELETE");
  }

  /**
   * Send request to Azure Key Vault API.
   *
   * @param string $type
   *   Request type to determine the API call URL.
   * @param string $resource_name
   *   Resource name to set in the API call (secret name).
   * @param string $http_request
   *   HTTP request type for the API call.
   * @param string $request_body
   *   Request body, if there is any.
   *
   * @return array
   *   The output or exception as decoded json array.
   */
  private function sendRequest($type, $resource_name, $http_request, $request_body = NULL) {
    $keyVault = $this->configService->getUrl();
    $apiVersionDate = $this->configService->getApiVersionDate();
    $apiVersion = $this->configService->getApiVersionNumber();

    switch ($type) {
      case "GET":
        $apiCall = "$resource_name";
        $keyVault = "";
        break;

      case "DELETE_SECRET":
        $apiCall = "secrets/$resource_name?api-version=$apiVersion";
        break;

      case "PURGE_SECRET":
        $apiCall = "deletedsecrets/$resource_name?api-version=$apiVersion";
        break;

      case "GET_SECRETS":
        $apiCall = "secrets?maxresults=25&api-version=$apiVersion";
        break;

      case "SECRET":
        $apiCall = "secrets/$resource_name?api-version=$apiVersion";
        break;

      case "KEY":
        $apiCall = "keys/$resource_name?api-version=$apiVersion";
        break;

      case "CREATE_KEY":
        $apiCall = "keys/$resource_name/create?api-version=$apiVersion";
        break;

      case "DELETE_KEY":
        $apiCall = "keys/$resource_name?api-version=$apiVersion";
        break;

      case "ENCRYPT_KEY":
        $apiCall = "keys/$resource_name/encrypt?api-version=$apiVersion";
        break;

      case "DECRYPT_KEY":
        $apiCall = "keys/$resource_name/decrypt?api-version=$apiVersion";
        break;
    }

    try {
      $result = $this->httpClient->request(
        $http_request,
        $keyVault . $apiCall,
        [
          'headers' => [
            'User-Agent' => 'browser/1.0',
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => "Bearer " . $this->accessToken,
          ],
          'json' => $request_body,
        ]
      );

      if ($result->getStatusCode() !== 200) {
        $this->logger->error("Error occurred while trying to do the Azure Key Vault API call %call. Status code: %status.",
          ['%call' => $apiCall, '%status' => $result->getStatusCode()]);
        throw new \Exception('API not returning 200.');
      }
      return json_decode($result->getBody()->getContents(), TRUE);
    }
    catch (ClientException $e) {
      $msg = t("Error occurred while trying to do the Azure Key Vault API call %call. More details: %details", [
        '%call' => $apiCall,
        '%details' => $e->getMessage()
      ]);
      \Drupal::messenger()->addMessage($msg, 'error', TRUE);

      $this->logger->error("Error occurred while trying to do the Azure Key Vault API call %call. More details: %details", [
        '%call' => $apiCall,
        '%details' => $e->getMessage()
      ]);
      return json_decode('{"value":"error_occured_while_trying_to_do_the_azure_key_vault_api_call_error"}', TRUE);
    }
  }

}
