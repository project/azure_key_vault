<?php

namespace Drupal\azure_key_vault\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Class AzVaultConfigForm.
 *
 * Configuration form for the Azure Key Vault API connection.
 */
class AzVaultConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'api_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['azure_key_vault.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->configFactory->get('azure_key_vault.settings');

    $form['azure_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Azure Key Vault URL'),
      '#description' => $this->t('The URL to your Azure key Vault.'),
      '#maxlength' => 256,
      '#size' => 128,
      '#weight' => '0',
      '#default_value' => $config->get('azure_url'),
      '#required' => TRUE,
    ];

    $form['token_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth token URL'),
      '#description' => $this->t('The URL to your authentication provider.'),
      '#maxlength' => 256,
      '#size' => 128,
      '#weight' => '0',
      '#default_value' => $config->get('token_url'),
      '#required' => TRUE,
    ];

    $form['vault_id'] = [
      '#type' => 'password',
      '#title' => $this->t('Vault Client ID'),
      '#maxlength' => 128,
      '#size' => 128,
      '#weight' => '1',
      '#default_value' => $config->get('vault_id'),
      '#required' => TRUE,
    ];

    $default_secret = $config->get('vault_secret');
    // Generate the URL for the link.
    $url = Url::fromUri('internal:/admin/config/system/keys/manage/' . $default_secret);
    $link = Link::fromTextAndUrl(t('Manage currently selected "@key" key.', ['@key' => $default_secret]), $url)->toString();
    if (is_null($default_secret) || empty($default_secret)) {
      $default_secret = 'vault_secret';
      $url = Url::fromUri('internal:/admin/config/system/keys/add');
      $link = '';
    }
    // Create a link object.
    $form['vault_secret'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Vault Client Secret'),
      '#weight' => '2',
      '#description' => $this->t('The selected key used to store the azure key vault client secret.'),
      '#default_value' => $default_secret,
      '#suffix' => $link,
      '#required' => TRUE,
    ];

    $form['api_version_date'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Version (date)'),
      '#description' => $this->t('Azure Key Vault API version (date)'),
      '#maxlength' => 128,
      '#size' => 128,
      '#weight' => '3',
      '#default_value' => $config->get('api_version_date'),
      '#required' => TRUE,
    ];

    $form['api_version_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Version (number)'),
      '#description' => $this->t('Azure Key Vault API version (number)'),
      '#maxlength' => 128,
      '#size' => 128,
      '#weight' => '4',
      '#default_value' => $config->get('api_version_number'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#weight' => '5',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->configFactory->getEditable('azure_key_vault.settings');

    $config->set('azure_url', $form_state->getValue('azure_url'))
      ->set('token_url', $form_state->getValue('token_url'))
      ->set('vault_id', $form_state->getValue('vault_id'))
      ->set('vault_secret', $form_state->getValue('vault_secret'))
      ->set('api_version_date', $form_state->getValue('api_version_date'))
      ->set('api_version_number', $form_state->getValue('api_version_number'))
      ->save(TRUE);

    parent::submitForm($form, $form_state);
  }

}
