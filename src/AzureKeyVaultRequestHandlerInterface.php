<?php

namespace Drupal\azure_key_vault;

/**
 * Interface AzureKeyVaultRequestHandlerInterface.
 *
 * Interface for the AzureKeyVaultRequestHandler.
 */
interface AzureKeyVaultRequestHandlerInterface {

  /**
   * Validate the details added in config area.
   *
   * @return bool
   *   Return true or false.
   */
  public function validateConfig();

  /**
   * Create secret in key vault with given name and value.
   *
   * @param string $secret_name
   *   Name for the secret.
   * @param string $value
   *   Value to store as the secret.
   *
   * @return array
   *   Return the created secret with metadata in an array.
   */
  public function createSecret(string $secret_name, string $value);

  /**
   * GET secret value from key vault.
   *
   * @param string $secret_name
   *   Name for the secret.
   *
   * @return string
   *   Value of the secret.
   */
  public function getSecret($secret_name);

  /**
   * GET a list of secrets from key vault.
   *
   * @return array
   *   List of secrets.
   */
  public function getSecrets();

  /**
   * Temporarily delete a secret from key vault (recorverable).
   *
   * @param string $secret_name
   *   Name for the secret.
   *
   * @return array
   *   Metadata of deleted secret.
   */
  public function deleteSecret($secret_name);

  /**
   * Purge (Permanently deletes) a secret from key vault.
   *
   * @param string $secret_name
   *   Name for the secret.
   *
   * @return array
   *   204 status code or error details.
   */
  public function purgeSecret($secret_name);

  /**
   * GET method for generic actions.
   *
   * @param string $full_url
   *   Complete URL for the API call.
   *
   * @return array
   *   Output for the API call.
   */
  public function getData($full_url);

  /**
   * Encrypts a key.
   *
   * @param string $key_to_be_encrypted
   *   Value to be encrypted.
   * @param string $key_name
   *   Name for the encrypted key.
   *
   * @return string
   *   Entrypted key value.
   */
  public function encryptKey($key_to_be_encrypted, $key_name);

  /**
   * Decrypts a key.
   *
   * @param string $encrypted_key
   *   Encrypted key string.
   * @param string $key_name
   *   Name of the encrypted key.
   *
   * @return string
   *   Decrypted value.
   */
  public function decryptKey($encrypted_key, $key_name);

  /**
   * GET key value from key vault.
   *
   * @param string $key_name
   *   Name of the encrypted key.
   *
   * @return string
   *   Encrypted key.
   */
  public function getKey($key_name);

  /**
   * GENERATE a RSA key in key vault.
   *
   * @param string $key_name
   *   Name of the encrypted key.
   * @param string $key_size
   *   Key size of the RSA key.
   *
   * @return string
   *   Encrypted key.
   */
  public function generateKey($key_name, $key_size);

  /**
   * DELETE a key from key vault.
   *
   * @param string $key_name
   *   Name of the encrypted key.
   *
   * @return array
   *   Metadata of deleted key.
   */
  public function deleteKey($key_name);

}
