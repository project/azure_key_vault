<?php

namespace Drupal\azure_key_vault;

use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\key\KeyRepository;

/**
 * Class AzureKeyVaultConfigurationService.
 *
 * Provide support fucntions to access API configuration settings.
 */
class AzureKeyVaultConfigurationService {

  /**
   * \Drupal\key\KeyRepository definition.
   *
   * @var \Drupal\key\KeyRepository
   */
  protected $keyService;

  /**
   * Drupal\Core\Config\ConfigManagerInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * Constructs a new AzureKeyVaultConfigurationService object.
   *
   * @param \Drupal\key\KeyRepository $key_repository
   *   The KeyRepository service.
   *
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The ConfigManagerInterface.
   */
  public function __construct(KeyRepository $key_repository, ConfigManagerInterface $config_manager) {
    $this->keyService = $key_repository;
    $this->configManager = $config_manager;
  }

  /**
   * Get Azure Key Vault URL.
   */
  public function getUrl() {
    return $this->configManager->getConfigFactory()->get('azure_key_vault.settings')->get('azure_url');
  }

  /**
   * Get OAuth token URL.
   */
  public function getTokenUrl() {
    return $this->configManager->getConfigFactory()->get('azure_key_vault.settings')->get('token_url');
  }

  /**
   * Get Vault Client ID.
   */
  public function getClientId() {
    return $this->configManager->getConfigFactory()->get('azure_key_vault.settings')->get('vault_id');
  }

  /**
   * Get Vault Client Secret.
   */
  public function getClientSecret() {
    $key_id = $this->configManager->getConfigFactory()->get('azure_key_vault.settings')->get('vault_secret');
    if (is_null($key_id)) {
      // Generate the URL from the path.
      $url_key_add = Url::fromUri('internal:/admin/config/system/keys/add');
      // Create a link object.
      $link_to_key = Link::fromTextAndUrl(t('secret key value'), $url_key_add)->toString();
      // Generate the URL from the route.
      $url = Url::fromRoute('azvault.admin');
      // Create a link object.
      $link = Link::fromTextAndUrl(t('Azure Key Vault Settings'), $url)->toString();
      // Set the error message with the link.
      \Drupal::messenger()->addError(t('Please select a secret option in the @link and also update your @link_to_key.', [
        '@link' => $link,
        '@link_to_key' => $link_to_key])
      );
      $key_id = 'vault_secret';
    }
    return $this->keyService->getKey($key_id)->getKeyValue();
  }

  /**
   * Get API Version Date.
   */
  public function getApiVersionDate() {
    return $this->configManager->getConfigFactory()->get('azure_key_vault.settings')->get('api_version_date');
  }

  /**
   * Get API Version Number.
   */
  public function getApiVersionNumber() {
    return $this->configManager->getConfigFactory()->get('azure_key_vault.settings')->get('api_version_number');
  }

}
