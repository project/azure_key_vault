<?php

namespace Drupal\azure_key_vault_key_provider\Plugin\KeyType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\key\Plugin\KeyPluginFormInterface;
use Drupal\key\Plugin\KeyTypeBase;
use Drupal\azure_key_vault\AzureKeyVaultRequestHandlerInterface;

/**
 * Defines a key type for encryption that generates keys with Azure Key Vault.
 *
 * @KeyType(
 *   id = "azure_encryption",
 *   label = @Translation("Azure Encryption"),
 *   description = @Translation("A key type used for encryption, generating keys using Azure Key Vault."),
 *   group = "encryption",
 *   key_value = {
 *     "plugin" = "generate"
 *   }
 * )
 */
class AzureEncryptionKeyType extends KeyTypeBase implements KeyPluginFormInterface {

  /**
   * Azure Key Vault Request Handler.
   *
   * @var \Drupal\azure_key_vault\AzureKeyVaultRequestHandlerInterface
   */
  protected $azkvHandler;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a new AzureEncryptionKeyType.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\azure_key_vault\AzureKeyVaultRequestHandlerInterface $azkv_handler
   *   The Azure key vault handler.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The Azure Key Vault Drupal logger channel.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AzureKeyVaultRequestHandlerInterface $azkv_handler,
    LoggerChannelInterface $logger
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->azkvHandler = $azkv_handler;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('azure_key_vault.http_client'),
      $container->get('logger.channel.azure_key_vault_key_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['key_size' => 2048];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $key_size_options = [
      '2048' => 2048,
      '3072' => 3072,
      '4096' => 4096,
    ];

    $key_size = $this->getConfiguration()['key_size'];

    $form['key_size'] = [
      '#type' => 'select',
      '#title' => $this->t('RSA key size'),
      '#description' => $this->t('The size of the key in bits.'),
      '#options' => $key_size_options,
      '#default_value' => $key_size,
      '#required' => TRUE,
    ];

    if (!$form_state->isSubmitted()) {
      $key_provider = $form_state->getUserInput()['key_provider'];
      if ($key_provider !== "azure_key_vault") {
        $form['provider_warning'] = [
          '#prefix' => '<p>',
          '#markup' => $this->t(
            '<span style="color:red;">Azure Encryption is supporting only the "Azure Key Vault" key provider, please select it or use another key type.</span>'
          ),
          '#suffix' => '</p>',
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    // Assign entered ID to the session as configurations not allowing access
    // it later.
    $entity_id = $form_state->getFormObject()->getEntity()->id();
    $_SESSION['key_name'] = $entity_id;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public static function generateKeyValue(array $configuration) {
    $key_size = $configuration['key_size'];
    $key_name = $_SESSION['key_name'];
    return \Drupal::service('azure_key_vault.http_client')->generateKey($key_name, (int) $key_size);
  }

  /**
   * {@inheritdoc}
   */
  public function validateKeyValue(array $form, FormStateInterface $form_state, $key_value) {
    if (!$form_state->getValue('key_size')) {
      return;
    }

    // Validate the key size.
    $bytes = $form_state->getValue('key_size') / 8;
    if (strlen($key_value) != $bytes) {
      // Doesn't work well with the Azure generated RSA keys,
      // disabling for now until rectify later.
      // $form_state->setErrorByName('key_size', $this->t('The selected key size does not match the actual size of the key.'));
    }
  }

}
