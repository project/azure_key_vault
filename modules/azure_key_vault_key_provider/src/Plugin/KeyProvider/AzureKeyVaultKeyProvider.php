<?php

namespace Drupal\azure_key_vault_key_provider\Plugin\KeyProvider;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\key\KeyInterface;
use Drupal\key\Plugin\KeyPluginFormInterface;
use Drupal\key\Plugin\KeyProviderBase;
use Drupal\key\Plugin\KeyProviderSettableValueInterface;
use Drupal\azure_key_vault\AzureKeyVaultRequestHandlerInterface;

/**
 * Adds a key provider that allows a key to be stored in Azure Key Vault.
 *
 * @KeyProvider(
 *   id = "azure_key_vault",
 *   label = "Azure Key Vault",
 *   description = @Translation("The Azure Key Vault key provider stores the key in Azure Key Vault key and secret management service."),
 *   storage_method = "azure_key_vault",
 *   key_value = {
 *     "accepted" = TRUE,
 *     "required" = TRUE
 *   }
 * )
 */
class AzureKeyVaultKeyProvider extends KeyProviderBase implements KeyProviderSettableValueInterface, KeyPluginFormInterface {

  /**
   * Azure Key Vault Request Handler.
   *
   * @var \Drupal\azure_key_vault\AzureKeyVaultRequestHandlerInterface
   */
  protected $azkvHandler;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a new AzureKeyVaultKeyProvider.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\azure_key_vault\AzureKeyVaultRequestHandlerInterface $azkv_handler
   *   The Azure key vault handler.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The Azure Key Vault Drupal logger channel.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AzureKeyVaultRequestHandlerInterface $azkv_handler,
    LoggerChannelInterface $logger
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->azkvHandler = $azkv_handler;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('azure_key_vault.http_client'),
      $container->get('logger.channel.azure_key_vault_key_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {

    // If this key type is for an encryption key.
    if ($form_state->getFormObject()->getEntity()->getKeyType()->getPluginDefinition()['group'] == 'encryption') {
      $configuration = $this->getConfiguration();
      $default_configuration = $this->defaultConfiguration();
      // Add an option to indicate that the value is stored Base64-encoded.
      $form['base64_encoded'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Base64-encoded'),
        '#description' => $this->t('Checking this will store the key with Base64 encoding.'),
        '#default_value' => $configuration['base64_encoded'] ?? $default_configuration['base64_encoded'] ?? 0,
      ];
    }

    $valid = $this->azkvHandler->validateConfig();
    if (!$valid) {
      $form['need_configure'] = [
        '#prefix' => '<p>',
        '#markup' => $this->t(
          '<span style="color:red;">This site has no active Azure Key Vault configuration, please <a href="@link">click here to configure</a>.</span>',
          ['@link' => Url::fromRoute('azvault.admin')->toString()]
        ),
        '#suffix' => '</p>',
      ];
    }

    if (!$form_state->isSubmitted()) {
      $key_type = $form_state->getUserInput()['key_type'];
      if ($key_type !== "authentication" && $key_type !== "azure_encryption" && $key_type !== "encryption") {
        $form['type_warning'] = [
          '#prefix' => '<p>',
          '#markup' => $this->t(
            '<span style="color:red;">Azure Key Vault is only supporting the "Authentication", "Encryption", "Azure Encryption" key types at this stage. Please change the key type to "Authentication", "Encryption" or "Azure Encryption" or choose a different key provider.</span>'
          ),
          '#suffix' => '</p>',
        ];
      }

      if ($key_type == "encryption") {
        $form['type_warning'] = [
          '#prefix' => '<p>',
          '#markup' => $this->t(
            '<span style="color:orange;">Please note that this will store the key in Azure Key Vault under "Secrets".</span>'
          ),
          '#suffix' => '</p>',
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $valid = $this->azkvHandler->validateConfig();
    if (!$valid) {
      $form_state->setErrorByName('key_provider', $this->t('Please configure your Azure Key Vault module with API access details.'));
    }

    $key = '';
    $user_inputs = $form_state->getUserInput();
    if (isset($user_inputs) && isset($user_inputs['id'])) {
      $key = $user_inputs['id'];
    }
    else {
      $key = $form_state->getFormObject()->getEntity()->id();
    }

    if (strpos($key, '_') !== FALSE) {
      $form_state->setErrorByName('label', $this->t('Please keep the "Key name" (specially, the machine name) without any spaces and / or special characters / symbols. Edit the "Machine name:" to remove all "_" characters too.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $base64_encoded = $form_state->getValue('base64_encoded');
    if ($base64_encoded !== NULL) {
      $this->setConfiguration(['base64_encoded' => $base64_encoded]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getKeyValue(KeyInterface $key) {
    try {
      $type = $key->getKeyType()->pluginId;
      if ($type === "authentication" || $type === "encryption") {
        $key_value = $this->azkvHandler->getSecret($key->id());
        if (isset($this->configuration['base64_encoded']) && $this->configuration['base64_encoded'] == TRUE) {
          $key_value = base64_decode($key_value);
        }
      }
      elseif ($type === "azure_encryption" || $type === "azure_key_import") {
        $key_value = $this->azkvHandler->getKey($key->id());
      }
      else {
        $key_value = FALSE;
      }
      return $key_value;
    }
    catch (\Exception $e) {
      $this->logException($e);
      return FALSE;
    }
  }

  /**
   * Creates a new key value, returning it.
   */
  protected function generateKey(KeyInterface $key) {
    $key_type = $key->getKeyType();
    if ($key_type->getPluginId() === 'azure_encryption') {
      $key_size = (int) $key_type->getConfiguration()['key_size'];
      $key_value = $this->azkvHandler->generateKey($key->id(), $key_size);
      return $key_value;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setKeyValue(KeyInterface $key, $key_value) {
    try {
      $type = $key->getKeyType()->pluginId;
      if ($type === "authentication" || $type === "encryption") {
        if (isset($this->configuration['base64_encoded']) && $this->configuration['base64_encoded'] == TRUE) {
          $key_value = base64_encode($key_value);
        }
        $this->azkvHandler->createSecret($key->id(), $key_value);
      }
      elseif ($type === "azure_key_import") {
        // @todo importKey: Main module need more work to import externally generated keys.
        // https://docs.microsoft.com/en-us/rest/api/keyvault/ImportKey
        // $this->azkvHandler->importKey($key->id(), $key_value);
        return FALSE;
      }
      else {
        return FALSE;
      }
      return TRUE;
    }
    catch (\Exception $e) {
      $this->logException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteKeyValue(KeyInterface $key) {
    try {
      $type = $key->getKeyType()->pluginId;
      if ($type === "authentication" || $type === "encryption") {
        $this->azkvHandler->deleteSecret($key->id());
      }
      elseif ($type === "azure_encryption" || $type === "azure_key_import") {
        $this->azkvHandler->deleteKey($key->id());
      }
      else {
        return FALSE;
      }
      return TRUE;
    }
    catch (\Exception $e) {
      $this->logException($e);
      return FALSE;
    }
  }

  /**
   * Logs exceptions that occur during Azure Key Vault requests.
   *
   * @param \Exception $e
   *   The exception to log.
   */
  protected function logException(\Exception $e) {
    $this->logger->error(
      'Error retrieving value from Azure Key Vault [{ex_code}]: {ex_msg}',
      [
        'ex_code' => $e->getCode(),
        'ex_msg' => $e->getMessage(),
      ]
    );
  }

}
